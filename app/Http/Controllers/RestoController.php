<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Restaurant;

class RestoController extends Controller
{
    function index()
    {
        return view('home');
    }
    function list()
    {
        $data=Restaurant::all();
        return view('list',["data"=>$data]);
    }
    function add(Request $r )
    {
        $add=new Restaurant;
        $add->name=$r->input('name');
        $add->email=$r->input('email');
        $add->address=$r->input('address');
        $add->save();
        return redirect('list')->with('success','Restaurant details entered successfully');;
    }
    
    function delete($id)
    {
        Restaurant::find($id)->delete();
        return back()->with('success','Restaurant Details deleted successfully');    
    }
    function edit($id)
    {
        $data=Restaurant::find($id);
        return view('edit',['data'=>$data]);
    }
    function update(Request $r,$id)
    {
        $add=Restaurant::find($id);
        $add->name=$r->input('name');
        $add->email=$r->input('email');
        $add->address=$r->input('address');
        $add->update();
        return back()->with('success','Updated successfully!');
    }
    function search(Request $r)
    {
       
            $q = $r->input('q');       
            $posts = Restaurant::query()
        ->where('name', 'LIKE', "%{$q}%")
        ->orWhere('email', 'LIKE', "%{$q}%")
        ->orWhere('address', 'LIKE', "%{$q}%")
        ->get();
    return view('search', compact('posts'));
            
    }
    
}
