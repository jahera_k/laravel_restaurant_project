@extends('layout')

@section('content')
<h2>Edit Restaurant Details</h2>

<form method="post" action="/edit/{{$data->id}}">
    @csrf
  <div class="form-group">
    <label>Name</label>
    <input type="text" name="name" class="form-control"  value="{{$data->name}}">
  </div>
  <div class="form-group">
    <label >Email</label>
    <input type="email" name="email" class="form-control" value="{{$data->email}}">
  </div>
  <div class="form-group">
    <label >Address</label>
    <input type="text" name="address" class="form-control"  value="{{$data->address}}">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@stop