@extends('layout')

@section('content')
<h1>Restaurant list page is here</h1>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Address</th>
    </tr>
  </thead>
  @foreach($data as $item)
  <tbody>
    <tr>
      <th scope="row">{{$item['id']}}</th>
      <td>{{$item['name']}}</td>
      <td>{{$item['email']}}</td>
      <td>{{$item['address']}}</td>
      <td>
        <a  href="/delete/{{$item->id}}"><i class="fa fa-trash"></i></a>
        <a  href="/edit/{{$item->id}}"><i class="fa fa-edit"></i></a>

      </td>
    </tr>
  </tbody>
  @endforeach
</table>

@stop