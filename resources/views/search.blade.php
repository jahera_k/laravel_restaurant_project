@extends('layout')

@section('content')
<h2>Search Details</h2>
<form action="{{ route('search') }}" method="get" role="search">
    {{ csrf_field() }}
    <div class="input-group">
        <input type="text" class="form-control" name="q"
            placeholder="Search...">
            <button type="submit" class="btn btn-default">Submit
            </button>
    </div>
</form>
@if($posts->isNotEmpty())
    <div class="container">
        <table class="table table-striped">

            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>
                @foreach($posts as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->address}}</td>
                </tr>
                @endforeach
            
            </tbody>
        </table>
    </div>
@else
    <div>
        <h2>No data found</h2>
    </div>
@endif
@stop