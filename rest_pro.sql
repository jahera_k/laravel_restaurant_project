-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2021 at 03:23 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rest_pro`
--

-- --------------------------------------------------------

--
-- Table structure for table `restaurants`
--

CREATE TABLE `restaurants` (
  `id` int(40) NOT NULL,
  `name` varchar(40) NOT NULL,
  `email` varchar(30) NOT NULL,
  `address` varchar(40) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `restaurants`
--

INSERT INTO `restaurants` (`id`, `name`, `email`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Arsalan', 'arsalan12@gmail.com', 'Kolkata', '2021-08-04 13:42:51', '2021-08-05 12:16:15'),
(3, 'domino\'s', 'dominos@gmail.com', 'Delhi', '2021-08-04 13:45:35', '2021-08-04 19:15:35'),
(6, 'Aminia', 'aminia@gmail.com', 'Kolkata', '2021-08-04 13:49:41', '2021-08-04 19:19:41'),
(7, 'Mio Amore', 'mioamore@gmail.com', 'Gujrat', '2021-08-04 09:19:27', '2021-08-04 14:49:27'),
(12, 'Pizza Hut', 'pizzahut@gmail.com', 'Mumbai', '2021-08-05 02:14:44', '2021-08-05 09:47:39'),
(13, 'KFC', 'kfc@test.com', 'Bangalore', '2021-08-05 02:17:14', '2021-08-05 07:47:14'),
(15, 'Dhaba', 'dhaba@icloude.com', 'Punjab', '2021-08-05 02:22:43', '2021-08-05 07:52:43'),
(16, 'Subway', 'subway@gmail.com', 'Delhi', '2021-08-05 07:02:03', '2021-08-05 12:32:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `id` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
